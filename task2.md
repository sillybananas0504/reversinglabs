# Release Notes for Z1000 Version 7.3.0



## New Features
1. **API to download original samples:** Users can now download original analyzed samples released to the public by Z1000, this feature is available through */api/v1/download_samples/*.
2. **Graphical Interface:** New graphical dashboard is available to showcase analyzed information about the analyzed emails!



## Bug Fixes
1. **Security issues with php files:**  A security issue where analysis of php files also lead to execution of the files has been fixed. In terms
of file permissions and access, potential security hazards could have occured had the bug fix not been implemented.
2. **Resolved instability caused by size overload:** The system no longer crashes if the attachment size exceeds 13.37GB. 

For more information on these features and bug fixes, please refer to https://example.com. 



